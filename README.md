Correr en local

```
make livehtml
```

## Documentación

### Sphinx

https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html

### Markdown

https://myst-parser.readthedocs.io/en/v0.13.5/using/syntax-optional.html

https://mystmd.org/guide/code

### Tema

Nos basamos en Sphinix Book:
https://sphinx-book-theme.readthedocs.io/en/stable/

Que a su vez se basa en PyData Theme:
https://pydata-sphinx-theme.readthedocs.io/en/latest/user_guide/index.html

### Dependencias

cross referencing:
https://docs.readthedocs.io/en/stable/guides/cross-referencing-with-sphinx.html

Resaltado de sintaxis de código:
https://pygments.org/docs/lexers/
