---
html_theme.sidebar_secondary.remove:
og:title: Solicitar una cuenta
---

# 🪪 Solicitar una cuenta

Para acceder a los recursos computacionales del CCAD
es necesario tener o [generar un par de llaves SSH](/tutoriales/ssh).

Luego deberá solicitar una cuenta completando el siguiente
[formulario](https://ccad.unc.edu.ar/servicios/pedido-de-cuentas/).

Una vez enviado el formulario, le llegará un email con su usuario
e [instrucciones para conectarse](conexion)
a los distintos [clusters disponibles](/infra/clusters/index).

Una vez que haya podido ingresar a alguno de los clusters,
puede usar [software preinstalado](/tutoriales/modules) por los administradores,
o [compilar sus propias aplicaciones](/tutoriales/spack) en su home.

Por último, debe [usar el gestor de trabajos](slurm) para ejecutar sus programas de manera óptima.

## Desde cero

Si nunca utilizó un cluster de cómputo o tiene poca experiencia en uso
de consola o sistemas Linux le recomendamos leer
[esta presentación](https://docs.google.com/presentation/d/e/2PACX-1vRJZN8GUK6rOSXuU27h4pBE2xLpQazTS3XGJ_KhJW2H2_qbQma5nBdpdp1Cm938pA/pub?start=false&loop=false&delayms=3000).

Está disponible también una versión informal en video dictada en el
marco de las «Friends of Friends Hybrid Meeting 2022» organizado por el
Observatorio Astronómico de Córdoba (OAC, UNC).

```{youtube} hZJjOQKmElg
:width: 100%
```
