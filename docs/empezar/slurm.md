---
og:title: Ejecución de trabajos
---

# 🚧 Ejecución de trabajos

Para ejecutar programas se usa [SLURM](http://slurm.schedmd.com/),
que es un sistema de gestión de trabajos
utilizado en clusters para distribuir y gestionar tareas de manera eficiente.

Para correr un trabajo, se necesita crear un [script de lanzamiento](/tutoriales/slurm-script)
que describa los recursos requeridos por el trabajo y las tareas que realiza.
Más abajo encontrará [ejemplos de scripts](#ejemplos).

```{seealso}
Para usar software ya instalados en el cluster, ver el [tutorial sobre modules](/tutoriales/modules).
```

## Comandos básicos

- **sbatch**: Para encolar un trabajo cuyo submit script es `job.sh`,
  luego de encolar el trabajo le devolverá un número que lo identifica.

  ```console
  $ sbatch job.sh
  Submitted batch job 1234
  ```

- **squeue**: Muestra la cola de trabajos pendientes o en ejecución.

  ```console
  $ squeue --me
  ```

- **sinfo**: Proporciona información sobre el estado de los nodos en el cluster.

  ```console
  $ sinfo
  ```

- **scancel**: Para eliminar un trabajo de la cola o cancelar su ejecución,
  usando el identificador del trabajo.

  ```console
  $ scancel 1234
  ```


## Ver trabajos encolados

Para ver todos los trabajos en las distintas colas de trabajos, utilice `squeue`.
El programa muestra distinta información sobre cada trabajo:

```{code-block} console
:caption: Ejemplo del comando squeue
$ squeue
PARTITION   JOBID PRIO       NAME     USER ST       TIME NO CPU  GRES NODELIST(REASON)
multi       55060 7353      seed1 usuario1 PD       0:00  4  80 (null (Resources)
mono        55079 6659     e05.e2 usuario2 PD       0:00  1   8 gpu:0 (AssocMaxJobsLimit)
multi       55081 6538       neb1 usuario3 PD       0:00  2  40 (null (Priority)
gpu         55008 6281     br_190 usuario4 PD       0:00  1   1 gpu:1 (AssocMaxJobsLimit)
multi       54915    0 Au3Ir5TiO2 usuario5 PD       0:00  2  40 (null (launch failed requeued held)
multi       55059 7347      seed3 usuario1  R       9:39  4  80 (null mendieta[11-12,19-20]
multi       54959 7167       neb2 usuario3  R 3-03:53:18  2  40 (null mendieta[13-14]
mono        55073 7038     e03.e2 usuario2  R    3:25:40  1   8 gpu:0 mendieta01
mono        55025 6777 e09.new.e2 usuario2  R 1-21:49:42  1   8 gpu:0 mendieta08
gpu         55007 6113     br_170 usuario4  R    3:25:53  1  16 gpu:1 mendieta05
```


:PARTITION: La cola en la que está
:JOBID: El identificador
:PRIO: La prioridad para que tiene para ser lanzado.
:NAME: El nombre.
:USER: El usuario que lo encoló.
:TIME: El tiempo que lleva ejecutando.
:NO: La cantidad de nodos que se estima que va a ocupar o que
  ya ocupa.
:CPU: La cantidad de cores totales que se estima que va a
  ocupar o que ya ocupa.
:GRES: Los aceleradores que pidió.
:NODELIST: Los nodos en los que está corriendo el
  trabajo, o la razón por la que no se está ejecutando todavía.
  Algunas razones comunes son:
    - **(Resources)**: No hay recursos libres en el cluster para ejecutarlo.
    - **(Priority)**: Hay trabajos con mayor prioridad esperando a ser ejecutados.
    - **(AssociationJobLimit)**: El usuario ya excede la cantidad de trabajos máxima en ejecución simultánea.
    - **(JobHeldUser)**: El usuario ha suspendido el trabajo.
    - **(JobHeldAdmin)**: Un administrador ha suspendido el trabajo.
    - **(launch failed requeued held)**: SLURM encontró un error al iniciar el trabajo y lo ha suspendido.
    - **(Nodes required for job are DOWN, DRAINED or reserved)**: Si alguno de los nodos que están down se levanta este job lo ocuparía.

## Ejemplos

### Hola Mundos

A continuación unos ejemplos básicos para probar y usar de base,
pero asegúrese de terminar de [configurar el resto de parametros](/tutoriales/slurm-script):

::::{tab-set}

:::{tab-item} 1 core
El script más simple posible: Corre en un sólo nodo,
lanza una sola task, que usa un sólo core
e imprime el nombre del nodo.
Funciona en cualquiera de los clusters.

```{code-block} slurm
:linenos:
:caption: job.sh
:name: hello-single-core
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1

srun bash -c 'echo "Hola, me ejecuté en el nodo $SLURMD_NODENAME"'
```

```{admonition} Tarea
:class: hint
Pruebe cambiar `#SBATCH --nodes=1` por `#SBATCH --nodes=2`.
```
:::

:::{tab-item} Serafin
:sync: serafin

Acá vamos a pedir y aprovechar un 1 nodo completo de Serafín.

El siguiente script representa nuestro programa:

```{code-block} bash
:linenos:
:name: hello.sh
:caption: hello.sh
#!/bin/bash

echo $(date): En el nodo $SLURMD_NODENAME soy la task $SLURM_PROCID
sleep 5 # "calculos"
echo $(date): task $SLURM_PROCID terminó
```

Este es el script de lanzamiento para sbatch:

```{code-block} slurm
:linenos:
:name: hello-submit-serafin
:caption: job.sh
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=64
#SBATCH --partition=short

srun hello.sh
```

Correrlo de la siguiente manera:

```{code-block} console
$ chmod +x hello.sh # solo una vez
$ sbatch job.sh
```

```{admonition} Tarea
:class: hint
Debe revisar los logs y ponga atención a los tiempos entre los dos `echo`s.

Pruebe cambiar `#SBATCH --nodes=1` por `#SBATCH --nodes=2`.
```

:::

:::{tab-item} Mendieta
:sync: mendieta

Pedir una GPU en Mendieta.

```{code-block} slurm
:linenos:
:name: hello-mendieta
:caption: job.sh
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --partition=short
#SBATCH --gpus-per-task=1

echo En el nodo $(hostname) tengo acceso a las siguientes GPUs:
nvidia-smi -L
```

:::

:::{tab-item} Eulogia
:sync: eulogia

Preámbulo para 2 nodos completos:

```{code-block} slurm
:name: hello-eulogia
:caption: job.sh
:linenos:
#!/bin/bash
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=64
#SBATCH --cpus-per-task=4
```
:::

:::{tab-item} Mulatona
:sync: mulatona

Preámbulo para 1 nodo completo:

```{code-block} slurm
:name: hello-mulatona
:caption: job.sh
:linenos:
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=32
#SBATCH --cpus-per-task=1
```
:::

::::



%TODO: cambiar a la version 7.2 y verificar que si lo copian tal cual va a funcionar
### Quantum Espresso

::::{tab-set}
:::{tab-item} Serafín
:sync: serafin

```{code-block} slurm
:caption: job.sh
:name: qe-serafin
:linenos:
#!/bin/bash
#SBATCH --job-name=espresso
#SBATCH --partition=multi
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=64
#SBATCH --cpus-per-task=1

# Cargar el entorno del usuario incluyendo la funcionalidad de modules
# No tocar
. /etc/profile

# Configurar OpenMP y otras bibliotecas que usan threads
# usando los valores especificados arriba
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export MKL_NUM_THREADS=$SLURM_CPUS_PER_TASK

# Cargar los módulos para la tarea
module load quantum-espresso/7.2

# Lanzar el programa
srun pw.x -nk 1 -inp input > output
```
:::

:::{tab-item} Eulogia
:sync: eulogia

```{code-block} slurm
:caption: job.sh
:name: qe-eulogia
:linenos:
#!/bin/bash
#SBATCH --job-name=espresso
#SBATCH --partition=multi
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=64
#SBATCH --cpus-per-task=1

. /etc/profile

# Configurar OpenMP y otras bibliotecas que usan threads
# usando los valores especificados arriba
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export MKL_NUM_THREADS=$SLURM_CPUS_PER_TASK

# Cargar los módulos para la tarea
module load quantum-espresso/7.1

# Lanzar el programa
srun pw.x -nk 1 -inp input > output
```
:::

::::

### GROMACS

::::{tab-set}

:::{tab-item} Serafín
:sync: serafin
```{code-block} slurm
:caption: job.sh
:name: gromacs-serafin
:linenos:
#!/bin/bash
#SBATCH --job-name=gmx
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=64
#SBATCH --partition=multi

. /etc/profile
module load gromacs
srun gmx_mpi mdrun -nb gpu -pme gpu -bonded gpu -update gpu -ntomp ${SLURM_CPUS_PER_TASK} (...)
```
:::

:::{tab-item} Mendieta
:sync: mendieta
```{code-block} slurm
:caption: job.sh
:name: gromacs-mendieta
:linenos:
#!/bin/bash
#SBATCH --job-name=gmx
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=10
#SBATCH --gpus-per-task=1
#SBATCH --partition=multi

. /etc/profile
module load gromacs
srun gmx_mpi mdrun -nb gpu -pme gpu -bonded gpu -update gpu -ntomp ${SLURM_CPUS_PER_TASK} (...)
```
:::

:::{tab-item} Eulogia
:sync: eulogia
```{code-block} slurm
:caption: job.sh
:name: gromacs-eulogia
:linenos:
#!/bin/bash
#SBATCH --job-name=gmx
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=64
#SBATCH --partition=multi

. /etc/profile
module load gromacs
srun gmx_mpi mdrun -nb gpu -pme gpu -bonded gpu -update gpu -ntomp ${SLURM_CPUS_PER_TASK} (...)
```
:::

::::


### Sesión interactiva

Se puede reservar un nodo y ejecutar comandos interactivamente
::::{tab-set}

:::{tab-item} Sin GPU
```{code-block} console
$ srun --pty bash
```
:::

:::{tab-item} Con GPU
```{code-block} console
:caption: Sólo en Mendieta!
$ srun --gpus=1 --pty bash
```
:::

::::

### Notebook Jupyter con GPU en Mendieta

``` 
conda activate <env>
salloc -p multi -N 1 --gres=gpu:1
srun hostname  # <= NRO_NODO
srun jupyter notebook --no-browser --port 9999 --ip 0.0.0.0

```

y luego

    ssh -L 9999:10.10.10.$NRO_NODO:9999 $USER@mendieta.ccad.unc.edu.ar

Una guía exaustiva sobre este tema fue elaborada por la usuaria Karen
Palacio y puede ser consultada
[aqui](https://docs.google.com/document/d/1IqPeMLI7RVKgkb5ZlVtYFLMlt6AKnbsi5fg9O3qEpq8).

## Preguntas Frecuentes

**¿Por qué SLURM no toma en cuenta el tiempo de ejecución solicitado?**

En las nuevas versiones de SLURM todas las directivas del preámbulo se
deben especificar en un solo bloque, es decir que apenas detecta un
comando SLURM considera que llegó al final del preámbulo. Por ejemplo,
el siguiente script considerará el tiempo de ejecución solicitado por el
usuario, en este caso 7 días:

```{code-block} slurm
#!/bin/bash
#SBATCH --job-name=test
#SBATCH --partition=mono
#SBATCH --ntasks=4
#SBATCH --cpus-per-task=2
#SBATCH --time 7-0:00:00
```

Mientras que el siguiente script tomará como tiempo de ejecución aquel
indicado en la configuración por defecto de la cola, porque la
instrucción `--time` no va a ser tomada en consideración:

```{code-block} slurm
#!/bin/bash
#SBATCH --job-name=test
#SBATCH --partition=mono
#SBATCH --ntasks=4
#SBATCH --cpus-per-task=2

export MYVAR="Hello"
#SBATCH --time 7-0:00:00
```

**¿Cómo sé si SLURM tomó en consideración todas las instrucciones
especificadas en el preámbulo?**

Es posible utilizar el comando **scontrol** para verificar que el gestor
de recursos haya encolado el trabajo de acuerdo a lo solicitado en el
preámbulo:

``` 
scontrol show job $JOBID 
```

Donde la variable $JOBID debe ser reemplazada por el numero del job.
