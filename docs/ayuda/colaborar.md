---
html_theme.sidebar_secondary.remove:
og:title: Enviar sugerencias
---

(sugerencias)=
# 🌟 Enviar sugerencias

Toda la documentación de esta wiki está alojada públicamente en [Gitlab](https://gitlab.com/ccad/wiki-rtd).

Si encontró un error o quiere colaborar escribiendo una sección sobre un tema particular,
no dude en abrir un _Issue_ o un _Merge Request_ para que entre todos mejoremos esta wiki.