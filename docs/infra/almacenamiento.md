---
og:title: Almacenamiento
---

# 🗄️ Almacenamiento

En los clusters conviven diferentes tipos de sistema de archivos:

```{figure} /_static/infra/nodo.png
```

## Sistemas de archivos locales

### /tmp

El directorio `/tmp` se encuentra accesible en todos los nodos de
cómputo. No se trata de un sistema de ficheros en disco sino
simplemente de una carpeta presente en la imagen del sistema. Por este
motivo, **la escritura de archivos en este directorio se desaconseja ya
que ocupa espacio en la memoria RAM del nodo**. La escritura de archivos
en /tmp está limitada al 50% de la memoria RAM total del nodo. Este
espacio se debe usar únicamente para la escritura de pequeños archivos
(como por ejemplo un file de estado o un file que indica la ejecución de
un programa). **Una vez terminado el trabajo, y si no existen otros
trabajos del mismo usuario en el nodo, los archivos creados en `/tmp` se
eliminan automáticamente.**

(infra-scratch)=
### /scratch

Los nodos cuentan con un sistema de archivos local montado en el
directorio `/scratch`. El mismo se encuentra en un volumen lógico
formateado con el sistema XFS y actualmente posee una capacidad de 192G
para mendieta, 208G para eulogia y 96G para mulatona.

Este espacio ha sido pensado para que los usuarios puedan escribir los
datos temporarios de sus cálculos con mejor rendimiento durante la
ejecución del trabajo sin tener que pasar por la red para escribir.
**Estos archivos se eliminan automáticamente al finalizar el trabajo** y
si quiere conservarlos deberá incluir una instrucción `sgather` al final
de su script de submit:

    ...
    srun <comando> 
    sgather /scratch/archivo_temporario prefijo

Esto copiará cada uno de los archivos con nombre "archivo\_temporario"
escritos en cada nodo durante la ejecución del comando. El destino de
los archivos serán carpetas con el prefijo seguidos del nombre del nodo
desde el que se copian los archivos. El comando cuenta con opciones para
copiar recursivamente carpetas y comprimir antes de copiar entre otras.
Puede consultar la documentación completa
[aqui](https://www.mankier.com/1/sgather)

## Sistemas de archivos de red

### /home

El directorio `/home`, como su nombre lo indica, alberga las carpetas
personales de los usuarios del cluster. Actualmente, posee una capacidad
de 50 Terabytes y se encuentra accesible a través de un montaje NFS. La
infraestructura que ofrece este servicio responde de manera
satisfactoria a las exigencias de los usuarios en términos de capacidad,
disponibilidad de los datos y seguridad. Sin embargo, en términos de
desempeño las capacidades en lectura y escritura disminuyen de manera
importante en caso de acceso concurrente (diferentes usuarios desde
varios nodos). Idealmente, este sistema de archivos debería utilizarse
unicamente para salvar los datos que se desean conservar una vez
terminado el trabajo de cálculo.
