---
og:title: Secure Shell
---

# 🔑 Secure Shell

SSH, o Secure Shell, es un protocolo seguro que permite acceder y administrar remotamente computadoras.

## Mecanismo de llave pública/privada

Haciendo una analogía con el mundo real, podríamos paragonar el
mecanismo de conexión al cluster mediante llaves SSH con la puerta de
nuestra casa. Este mecanismo se compone de un par de archivos llamados
*public key* y *private key*. El archivo público (*public key*)
representa la cerradura de la puerta mientras que el archivo privado
(*private key*) es la llave que abre dicha cerradura. Así como es
posible poner la misma cerradura a muchas puertas y usar la misma llave
para entrar a todas copiando la *public key* en varios servidores, es
también factible poner varias cerraduras en la misma puerta y abrir la
misma con diferentes llaves copiando varias *public keys* en el mismo
servidor.

Principios básicos para tener en cuenta:

- la *private key* debe permanecer siempre bajo custodia de la misma
manera que custodiamos la llave de nuestra casa,
- en NINGUN CASO se debe comunicar el contenido de la *private key*,
- en caso de reinstalación del sistema operativo recordarse de hacer
una copia de la *private key*,
- en caso de pérdida de la *private key* (por ejemplo luego de la
reinstalación del sistema) comunicar a los administradores del
cluster la nueva *public key*,
- un cluster es un sistema de recursos computacionales compartidos, la
seguridad del mismo depende en gran medida del uso responsable de
los mecanismos de autenticación. En caso de hurto o robo de la
*private key* (o de la computadora en la cual se encuentra
almacenada) comunicar intempestivamente el episodio a [soporte](soporte).

Para más información acerca de las claves publicas y privadas:

```{youtube} AQDCe585Lnc
:width: 100%
```

(generate-ssh-keys)=
## Generar llaves ssh

### Con Linux

Puede generar un nuevo par de llaves SSH desde la terminal de un sistema
GNU/Linux ejecutando el siguiente comando:

```{code-block} console
$ ssh-keygen -b 4096
```

```{tip}
Presionar _enter_ para aceptar las opciones por defecto.
```

La clave pública es el archivo "id_rsa.**pub**".
Para ver el contenido puede ejecutar `cat` y copiar su contenido:

```{code-block} bash
:emphasize-lines: 1
$ cat .ssh/id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAklOUpkDHrfHY17SbrmTIpNLTGK9Tjom/BWDSU
GPl+nafzlHDTYW7hdI4yZ5ew18JH4JW9jbhUFrviQzM7xlELEVf4h9lFX5QVkbPppSwg0cda3
Pbv7kOdJ/MTyBlWXFCR+HAo3FXRitBqxiX1nKhXpHAZsMciLq8V6RjsNAQwdsdMFvSlVK/7XA
t3FaoJoAsncM1Q9x5+3V0Ww68/eIFmb1zuUFljQJKprrX88XypNDvjYNby6vw/Pb0rwert/En
mZ+AW4OZPnTPI89ZPmVMLuayrD2cE86Z/il8b+gw3r3+1nKatmIkjn2so1d01QraTlMqVSsbx
NrRFi9wrf+M7Q== user@ejemplo
```

### Con Windows

Para usuarios de Windows se recomienda seguir el siguiente procedimiento:

1. Descargar Mobaxterm desde la pagina de [descarga](http://mobaxterm.mobatek.net/download-home-edition.html)
eligiendo la versión instalable.

    % TODO: estoy asumiendo, no probé
1. Descomprimir el zip.

1. Ejecutar el instalador y completar la instalación.

1. Ejecutar el programa MobaXterm.

1. Dentro de la terminal que ofrece el programa lanzar los siguientes comandos:

    ```console
    $ ssh-keygen -b 4096
    $ cat .ssh/id_rsa.pub
    ```

```{warning}
Desaconsejamos el uso de la versión portable, ya que genera problemas con las llaves.
```

## Comunicar la *llave pública* a los administradores

Las llaves generadas por el comando *ssh-keygen* se almacenan por
defecto en el directorio `$HOME/.ssh/` de la maquina desde la que nos
queremos conectar. Los ficheros {file}`id_rsa` e {file}`id_rsa.{pub}` contienen
respectivamente las llaves privada y pública. Para poder garantizar el
acceso al cluster los administradores necesitan el contenido del fichero
id_rsa.**pub**. El mismo se puede obtener gracias al comando cat:

```console
$ cat $HOME/.ssh/id_rsa.pub
```

O bien puede enviar directamente el fichero `$HOME/.ssh/id_rsa.pub` como
adjunto por mail.

## Acceder al cluster desde otra PC

Para acceder al cluster desde una computadora distinta a la que usó para
[generar las llaves SSH](generate-ssh-keys) existen dos maneras de hacerlo:

1. Copiar la llave **privada**, es decir, transferir
el fichero `$HOME/.ssh/id_rsa` original a la nueva computadora.

2. [Generar un par de llaves ssh](generate-ssh-keys) en la nueva computadora
y copiar la llave **pública** al cluster. 

    Para ello se debe copiar el contenido de la llave pública recién generada (`$HOME/.ssh/id_rsa.pub`),
    al fichero `$HOME/.ssh/authorized_keys` que se encuentra en el cluster
    usando la computadora que usa habitualmente.

    Si por alguna razón no puede conectarse al cluster, puede comunicar la
    nueva _clave pública_ a [soporte](soporte).
