---
og:title: Instalar software con Spack
---

# ⛏️ Instalar software con Spack

[Spack](https://spack.io/) es un gestor de paquetes centrado específicamente en las necesidades de la computación científica y de alto rendimiento.
Facilita la compilación, instalación, y la gestión de herramientas necesarias para proyectos científicos y de simulación.

## Descarga

Para bajar spack debe clonar el repositorio

```console
$ git clone --branch releases/v0.21 https://github.com/spack/spack.git ~/spack
```

```{note}
El flag `--branch releases/v0.21` es para usar la última versión 0.21.x de spack.
Para usar la última version, ver [las releases en GitHub](https://github.com/spack/spack/releases).
```

## Configuración

Cargue spack en la sesión

```console
$ source ~/spack/share/spack/setup-env.sh
```

Cargue el compilador GCC y configuraciones por defecto de spack
```console
$ module load gcc spack-config
```

Indique a spack que busque los compiladores disponibles
```console
$ spack compiler find
```

## Compilación

Para compilar e instalar, por ejemplo, OpenMX

```console
$ spack install openmx
```

También puede indicar exactamente que versión del compilador usar

```console
$ spack install openmx %gcc@11.2
```

y esperar...

```{tip}
Hacer esto dentro de [tmux](/tutoriales/tmux) o `screen` porque puede tardar mucho.
```

Si la compilación fue exitosa, va a poder hacer lo siguiente:
```console
$ spack module tcl refresh -y
==> Regenerating tcl module files
$ module use ~/spack/share/spack/modules/linux-rocky8-zen2/
$ module avail
----------------------------------- /home/aisilva/spack/share/spack/modules/linux-rocky8-zen2 -----------------------------------
[...]
hcoll-4.8.3220-gcc-11.2.0-7enxbnq       openmpi-4.1.2-gcc-11.2.0-ghlcelo           xz-5.4.1-gcc-11.2.0-dvtphbi            
hwloc-2.9.1-gcc-11.2.0-dteec3g          openmx-3.9-gcc-11.2.0-ibaapqx              zlib-1.2.13-gcc-11.2.0-6e3573b         
knem-1.1.4.90-gcc-11.2.0-ahbed4n        openssh-8.0p1-gcc-11.2.0-ujsbtre
[...]

$ module load openmx-3.9-gcc-11.2.0-ibaapqx
Loading openmx-3.9-gcc-11.2.0-ibaapqx
  Loading requirement: [...]
$ which openmx
~/spack/opt/spack/linux-rocky8-zen2/gcc-11.2.0/openmx-3.9-ibaapqx5jikmslor2wui2hgfldxgicj7/bin/openmx
```

```{seealso}
[Más detalles sobre como funciona el comando `module`](/tutoriales/modules).
```

Escriba el `submit.sh`

```{code-block} slurm
:emphasize-lines: 14,15
#!/bin/bash
#SBATCH --job-name=test
#SBATCH --partition=short
#SBATCH --ntasks-per-node=64
#SBATCH --cpus-per-task=1
#SBATCH --nodes=2
#SBATCH --time 0-1:00

source /etc/profile

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export MKL_NUM_THREADS=$SLURM_CPUS_PER_TASK

module use ~/spack/share/spack/modules/linux-rocky8-zen2/
module load gcc openmx-3.9-gcc-11.2.0-ibaapqx

srun --mpi=pmix openmx test_1.dat > test_1.std
```

```{seealso}
[Como escribir un script de submit](/tutoriales/slurm-script).
```

Y ya puede encolar el job.
```console
$ squeue submit.sh
```

```{seealso}
Un tutorial más completo en inglés: [spack-tutorial.readthedocs.io](https://spack-tutorial.readthedocs.io/en/latest/).
```
