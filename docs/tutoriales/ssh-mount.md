---
og:title: Gestión de datos a distancia
---

# 🪃 Gestión de datos a distancia

Es posible montar localmente el directorio `/home/$USUARIO` del cluster y
de este modo realizar todas las operaciones sobre los archivos (copiar,
renombrar, editar, etc.) con las herramientas de preferencia instaladas
en su PC de trabajo. Para ello existen diferentes métodos. A
continuación presentamos los 3 más utilizados. **Todos estos métodos
funcionaran únicamente si las llaves SSH están correctamente
configuradas.**

## Desde la interfaz gráfica

Los usuarios de GNOME o KDE pueden montar el directorio
home del cluster directamente desde la interfaz gráfica. Los pasos
a seguir son:

**GNOME (Nautilus)**

1. Abrir un gestor de archivos

1. Hacer click en {menuselection}`+ Otras ubicaciones --> Conectarse al servidor`

1. En el campo poner:

    ::::{tab-set}
    :::{tab-item} Serafín
    ```{code-block}
    ssh://serafin.ccad.unc.edu.ar
    ```
    :::
    :::{tab-item} Mendieta
    ```{code-block}
    ssh://mendieta.ccad.unc.edu.ar
    ```
    :::
    :::{tab-item} Eulogia
    ```{code-block}
    ssh://eulogia.ccad.unc.edu.ar
    ```
    :::
    :::{tab-item} Mulatona
    ```{code-block}
    ssh://mulatona.ccad.unc.edu.ar
    ```
    :::
    ::::

1. Hacer clic en {guilabel}`Conectar`.

1. En el cuadro de dialogo completar el usuario, dejando en blanco la contraseña, y hacer clic en {guilabel}`Conectar`.

```{tip}
Para que quede configurada la conexión, puede hacer {menuselection}`Clic secundario --> Añadir marcador`.
```

## Con sshfs

En primer lugar se debe crear en el sistema cliente un directorio que
utilizaremos a continuación como punto de montaje local:

```console
$ mkdir $HOME/datos_mendieta
```

Luego se realiza el montaje reemplazando la variable $USER por el nombre
de usuario asignado en el cluster:

```console
$ sshfs $USER@mendieta.ccad.unc.edu.ar:/home/$USER $HOME/datos_mendieta
```

Comprobar que el directorio se ha montado correctamente y que se poseen
los permisos para operar en el sistema remoto:

```console
$ ls $HOME/datos_mendieta
$ touch $HOME/datos_mendieta/puedoescribir
$ rm $HOME/datos_mendieta/puedoescribir
```

Para desmontar el directorio se utiliza el siguiente comando:

```bash
fusermount -u $HOME/datos_mendieta
```

## Al arranque

Es posible configurar el sistema cliente para que monte automáticamente
el directorio home del cluster al arranque. Para ello debemos seguir
algunos pasos muy sencillos. En primer lugar asegurarse que el usuario
pertenece al grupo *fuse* o bien que puede acceder en lectura al fichero
*/etc/fuse.conf*. El fichero */etc/fuse.conf* debe contener la siguiente
instrucción:

    user_allow_other

Si no es el caso, editar el fichero con el superusuario root.

Una vez hecho esto, agregar la siguiente linea al fichero */etc/fstab* :

```
$USER@mendieta.ccad.unc.edu.ar:/home/$USER  $MOUNTPOINT fuse.sshfs auto,_netdev,user,idmap=user,transform_symlinks,identityfile=$HOME/.ssh/id_rsa,allow_other,default_permissions, 0 0
```

El reemplazo de las variables se hará de esta manera:

$USER es el nombre de usuario en el cluster mendieta

$MOUNTPOINT es el nombre del directorio local donde se quieren montar
los datos remotos. Debe contener el path absoluto, por ejemplo
*/home/maradona/datos_mendieta*

$HOME es el directorio home del usuario local donde se encuentra la
llave SSH privada.
